<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DbPoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_point', function (Blueprint $table) {
            $table->id('point_id');
            $table->string('point_name');
            $table->string('point_slug');
            $table->integer('point_bhyt');
            $table->integer('point_dichvu');
            $table->integer('point_dichvutyc');
            $table->integer('group_id');
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_point');
    }
}
