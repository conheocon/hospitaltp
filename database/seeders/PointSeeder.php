<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
        	[
        		'point_name'=>'Khám bệnh trong giờ',
        		'point_slug'=>'khambenhtronggio',
        		'point_bhyt'=>34000,
        		'point_dichvu'=>34000,
        		'point_dichvutyc'=>0
        	],
        ];
        DB::table('db_point')->insert($data);
    }
}
