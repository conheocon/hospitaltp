<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PointController;
use App\Http\Controllers\KhambenhController;
use App\Http\Controllers\PhauthuatController;
use App\Http\Controllers\TiemchungController;
use App\Http\Controllers\CanlamsanController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminPointController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Chỉ định - Admin
Route::group(['prefix' => 'point','middleware'=>'auth'], function(){
	Route::get('/', [AdminPointController::class,'view_addpoint'])->name('admin_viewaddpoint');
	//Chọn nhóm đổ ra khoa
	Route::post('/select-nhomkhoa', [AdminPointController::class, 'select_nhomkhoa'])->name('select_nhomkhoa');
	Route::post('/addpoint', [AdminPointController::class, 'insert_point'])->name('admin_addpoint');

	Route::get('/show-point', [AdminPointController::class, 'show_point'])->name('show_point');
	Route::get('/edit-point/{point_id}', [AdminPointController::class, 'edit_point'])->name('edit_point');
	Route::post('/update-point/{point_id}', [AdminPointController::class, 'update_point'])->name('update_point');
	Route::get('/delete-point/{point_id}', [AdminPointController::class, 'delete_point'])->name('delete_point');

	//Tìm kiếm tự động
	// Route::post('/autocomplete-ajax2', [AdminPointController::class,'autocomplete_ajax2'])->name('autocomplete_ajax2');
	
	//Search_point
	Route::post('/search-point', [AdminPointController::class, 'search_point'])->name('admin_searchpoint');

	//UploadfilePDF
	Route::get('/view-uploadpdf', [AdminPointController::class, 'view_uploadpdf'])->name('view_uploadpdf');
	Route::post('/import-pdf', [AdminPointController::class, 'import_pdf'])->name('import_pdf');
	Route::get('/view-files/{files_id}', [AdminPointController::class, 'view_files'])->name('view_files');
	Route::get('/delete-viewpdf/{files_id}', [AdminPointController::class, 'delete_viewpdf'])->name('delete_viewpdf');
});

Route::get('/',function ()
{
	return view('index');
})->name('index');

Route::get('/templatesearch', [PointController::class,'templatesearch'])->name('search_point');

Route::post('/search-keywords-point', [PointController::class,'search_keywords_point'])->name('search_keywords_point');

Route::post('/autocomplete-ajax', [PointController::class,'autocomplete_ajax'])->name('autocomplete_ajax');

//ấn vào li => pop up
Route::post('/autocomplete', [PointController::class,'autocomplete'])->name('autocomplete');

//Khám bệnh - khám sức khỏe
Route::get('/khambenh', [KhambenhController::class,'templatekhambenh'])->name('khambenh');
 
//Phẫu thuật - thủ thuật
Route::get('/phauthuat', [PhauthuatController::class,'templatephauthuat'])->name('phauthuat');

//Tiêm chủng - giá phòng
Route::get('/tiemchung', [TiemchungController::class,'templatetiemchung'])->name('tiemchung');

//Cận lâm sàn
Route::get('/canlamsan', [CanlamsanController::class,'templatecanlamsan'])->name('canlamsan');

//importexcel
Route::get('/importexcel', [PointController::class,'importexcel'])->name('importexcel');
Route::post('/import-excel', [PointController::class,'import_excel'])->name('importzexcel');
Route::post('/export-excel', [PointController::class,'export_excel'])->name('exportzexcel');
