<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Point;
use App\Models\Category;
use App\Models\Group;
use App\Models\Procedure;
use App\Http\Requests;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();

class AdminPointController extends Controller
{
    //
    public function view_addpoint(Request $request){

    	$point_group = Group::orderby('id','ASC')->get();
    	return view('admin.point.add_point')->with(compact('point_group'));
    }
    //Chọn nhóm -> khoa
    public function select_nhomkhoa(Request $request){
    	$data = $request->all();
    	if($data['action']){
    		$output = '';
    		if($data['action']=="group_point"){
    			$select_khoa = Category::where('group_id',$data['groups_id'])->orderby('id','ASC')->get();
    			foreach ($select_khoa as $key => $khoa){
    				$output .='<option value="'.$khoa->id.'">'.$khoa->name.'</option>';
    			}
    		}
    	}
    	echo $output;
    }

    public function insert_point(Request $request){
    	$data = $request->all();
        $point = new Point();
        $point->name = $data['name'];
        $point->slug = str::slug($data['name']);
        $point->price_bhyt = $data['price_bhyt'];
        $point->price_dichvu = $data['price_dichvu'];
        $point->price_dichvutyc = $data['price_dichvutyc'];
        $point->group_id = $data['group_id'];
        $point->category_id = $data['category_id'];
        $point->save();
        Session::put('message','Thêm chỉ định thành công!!!');
        return Redirect::to('point');  
    }

    public function show_point(){
    	$all_point = Point::orderby('id','desc')->paginate(15);
    	$point_category = Category::all();
    	return view('admin.point.show_point')->with(compact('all_point','point_category'));
    }

    public function delete_point($point_id){
    	Point::destroy($point_id);
    	Session::put('message','Xóa chỉ định thành công');
    	return Redirect::to('point/show-point');
    }

    public function edit_point(Request $request, $point_id){
    	$point_group = Group::orderby('id','ASC')->get();
    	$edit_point = Point::find($point_id);
    	return view('admin.point.edit_point')->with(compact('point_group','edit_point'));
    }

    public function update_point(Request $request, $point_id){
    	$point = Point::find($point_id);
    	$data = $request->all();
        $point->name = $data['name'];
        $point->slug = str::slug($data['name']);
        $point->price_bhyt = $data['price_bhyt'];
        $point->price_dichvu = $data['price_dichvu'];
        $point->price_dichvutyc = $data['price_dichvutyc'];
        $point->group_id = $data['group_id'];
        $point->category_id = $data['category_id'];
        $point->save();
        Session::put('message','Sửa chỉ định thành công!!!');
        return Redirect::to('point/show-point');  
    }

    //Tìm kiếm tự động
    // public function autocomplete_ajax2(Request $request){
    // 	$data = $request->all();
    //     if(strlen($data['query'])>=2){
    //         $point = Point::where('name','like','%'.$data['query'].'%')->get();
    //     $output = '
    //         <ul class="dropdown-menu" id="dropdownmenu1" >
    //     ';
    //     foreach ($point as $key => $value) {
    //         $output .='
    //             <li class="li_search_ajax" style="margin-left: 5px; margin-right: 5px" data-toggle="modal" data-target="#exampleModalLong" data-id="'.$value->id.'" data-name="'.$value->name.'"><a href="#" >'.$value->name.'</a></li>
    //             ';
    //     }
    //     $output .= '</ul>';
    //     echo $output;
    //     }
    // }

    //Tìm kiếm chỉ định
    public function search_point(Request $request){
        $tukhoa = $request->search_keywords;
        $khoa = $request->khoa_name;

        if($tukhoa){
            $data_keywords = Point::where('name','like','%'.$tukhoa.'%')->get();
        }
        if($khoa){
            $data_keywords = Point::where('category_id',$khoa)->get();
        }
        if($tukhoa == null && $khoa || $tukhoa == '' && $khoa){
            $data_keywords = Point::where('category_id',$khoa)->get();
        }
        if($tukhoa && $khoa){
            $data_keywords = Point::where('name','like','%'.$tukhoa.'%')->where('category_id',$khoa)->get();
        }
        if($tukhoa == null && $khoa == null || $tukhoa == '' && $khoa == null){
            $data_keywords = Point::all();
        }

    	$point_category = Category::all();
    	return view('admin.point.search_point')->with(compact('point_category','data_keywords'));
    }

    //UploadPDF
    public function view_uploadpdf(){
        $file = Procedure::paginate(10);
        return view('admin.quytrinh.view_uploadfilepdf',compact('file'));
    }

    public function import_pdf(Request $request){
        $this->validate($request, [
        'file' => 'required|mimes:pdf'
        ]);

        $procedure = new Procedure();
        $file = $request->file('file');
        $get_name_image = $file->getClientOriginalName();
        $name_image = current(explode('.',$get_name_image));
        $fileName = $name_image.'.'.$file->getClientOriginalExtension();
        $request->file->move('storage/',$fileName);

        $procedure->file = $fileName;
        $procedure->save();
        Session::put('message','Thêm quy trình thành công');
        return redirect()->back();
    }

    public function view_files($files_id){
        $data = Procedure::find($files_id);
        return view('admin.quytrinh.seen',compact('data'));
    }

    public function delete_viewpdf($files_id){
        Procedure::destroy($files_id);
        Session::put('message','Xóa quy trình thành công');
        return Redirect::to('point/view-uploadpdf');
    }

}