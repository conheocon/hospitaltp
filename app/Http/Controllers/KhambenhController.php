<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Point;
use App\Models\Category;
use App\Models\Group;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class KhambenhController extends Controller
{
    public function templatekhambenh (){
      $point_group = Group::find(22);
      // $point_group_id = $point_group->id;
      $point_point = Point::where('group_id',22)->paginate(10);
      
      $point_category = Category::where('group_id',22)->get();
      return view('khambenh',compact('point_category','point_point'));
    }
}
