<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Point;
use App\Models\Category;
use App\Models\Group;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class PhauthuatController extends Controller
{
    //
    public function templatephauthuat (){
    	$point_group = Group::where('id',23)->first();
    	$point_group_id = $point_group->id;
    	$point_point = Point::where('group_id',$point_group_id)->paginate(10);
    	// dd($point_point->name);
    	// dd($point_group->point->name);
      $point_category = Category::where('group_id',$point_group_id)->get();
      return view('phauthuat')->with(compact('point_category','point_point'));
    }
}
