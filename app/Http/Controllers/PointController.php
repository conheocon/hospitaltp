<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Point;
use App\Models\Category;
use App\Models\Group;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

use App\Imports\ExcelImports;
use App\Exports\ExcelExports;
use Excel;

class PointController extends Controller
{
    public function templatesearch (){
      $point_all = Point::paginate(10);
      $point_category = Category::all();
    	return view('search')->with(compact('point_all','point_category'));
    }

    public function search_keywords_point (Request $request){

    	$search = $request->input('search_keywords');
      $category_id = $request->input('category_id');

    	$search_point = Point::where('name','like','%'.$search.'%');

      if($category_id){
        $search_point = $search_point->where('category_id',$category_id)->get();
      }
      if($search == null || $search == ''){
        $search_point = Point::where('category_id',$category_id)->get();
      } else {
        $search_point= $search_point->orWhere(function ($query) use ($search) {
                            $query->where('name','like',$search.'%')
                                  ->where('name','like','%'.$search);
                          })->get();
      }

    	$output = '';
    	$output .='<div class="modal-body">
    				 <table class="table table-hover">
	                  <thead class="content-font1" style="font-size: 15px">
	                      <tr>
	                          <th rowspan="2">Tên chỉ định (Theo phương tương đương)</th>
                            <th rowspan="2">Khoa</th>
	                          <th colspan="3">Giá</th>
	                      </tr>
	                      <tr>
	                          <th scope="col"> BHYT <br> (Theo TT13) </th>
	                          <th scope="col"> Dịch vụ <br> (Theo TT14) </th>
	                          <th scope="col">Dịch vụ theo <br> yêu cầu <br> (Ngoài giờ)</th>
	                      </tr>
	                      <tr class="one">
                            <th></th>
	                          <th></th>
	                          <th></th>
	                          <th></th>
	                          <th></th>
	                      </tr>
	                  </thead>
	                  <tbody class="content-font2" style="font-size: 15px">
	               ';

      // if ($search_point->count()>0){
      if ($search_point->isNotEmpty()){
         foreach ($search_point as $key => $value) {
         		$output.='
         			<tr>
                 <td scope="row">'.$value->name.'</td>
                 <td>'.$value->category->name.'</td>
                 <td>'.$value->price_bhyt.' </td>
                 <td>'.$value->price_dichvu.' </td>
                 <td>'.$value->price_dichvutyc.' </td>
              </tr>
         		';
         }
      }else{
        $output.='
            <tr>
               <td style="text-align: center" colspan="5">Không tìm thấy chỉ định nào</td>
            </tr>
        ';
      }

     $output .='
     		</tbody>
           </table>
          </div>
     ';
    	return response()->json(['output'=>$output]);
    }
    
    public function search (Request $request){
    	$keywords = $request->search_keywords;
    	$search_point = Point::where('name','like','%'.$keywords.'%')
                            ->orWhere(function ($query) use ($keywords) {
                              $query->where('name','like',$keywords.'%')
                                    ->where('name','like','%'.$keywords);
                            })
                            ->take(9)
                            ->get();
    	return view('search')->with(compact('search_point'));
    }

    public function autocomplete_ajax(Request $request){
        $data = $request->all();
        if(strlen($data['query'])>=2){
            $point = Point::where('name','like','%'.$data['query'].'%');
            if($request->input('category') != null){
              $category_id = $request->input('category');
              $category_id = (int)$category_id;
              $point = $point->where('category_id',$category_id);
            }
            $point = $point->orWhere(function ($query) use ($data) {
                              $query->where('name','like',$data['query'].'%')
                                    ->where('name','like','%'.$data['query']);
                          });
            $point = $point->get();
        $output = '
            <ul class="dropdown-menu" id="dropdownmenu">
        ';
        foreach ($point as $key => $value) {
            $output .='
                <li class="li_search_ajax" style="margin-left: 5px; margin-right: 5px" data-toggle="modal" data-target="#exampleModalLong" data-id="'.$value->id.'" data-name="'.$value->name.'"><a href="#" >'.$value->name.' </a>(Khoa : '.$value->category->name.' )</li>
                ';
        }

        $output .= '</ul>';
        echo $output; 
        }
    }

    public function autocomplete(Request $request) {
        $search = $request->input('query1');
        $search_point = Point::where('id',$search)->get();
        $output = '';
        $output .='<div class="modal-body"> 
                     <table class="table table-hover">
                      <thead class="content-font1" style="font-size: 15px">
                          <tr>
                              <th rowspan="2">Tên chỉ định (Theo phương tương đương)</th>
                              <th rowspan="2">Khoa</th>
                              <th colspan="3">Giá</th>
                          </tr>
                          <tr>
                              <th scope="col"> BHYT <br> (Theo TT13) </th>
                              <th scope="col"> Dịch vụ <br> (Theo TT14) </th>
                              <th scope="col">Dịch vụ theo <br> yêu cầu <br> (Ngoài giờ)</th>
                          </tr>
                          <tr class="one">
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody class="content-font2" style="font-size: 15px">
                   ';

                   foreach ($search_point as $key => $value) {
                        $output.='
                            <tr>
                               <td scope="row">'.$value->name.'</td>
                               <td>'.$value->category->name.'</td>
                               <td>'.$value->price_bhyt.'</td>
                               <td>'.$value->price_dichvu.'</td>
                               <td>'.$value->price_dichvutyc.'</td>
                            </tr>
                        ';
                   }

                   $output .='
                        </tbody>
                       </table>
                      </div>
                   ';
        return response()->json(['output'=>$output]);
    }

    //-----------------------------------------------
    //viewexcel
    public function importexcel(){
        return view('excel');
    }

    public function import_excel(Request $request){
      $this->validate($request, [
        'file' => 'required|mimes:xls,xlsx'
      ]);
      $path = $request->file('file');
      $array = Excel::toCollection(new ExcelImports, $path);
      foreach ($array as $key => $chidinh) {
        foreach ($chidinh as $key => $value) {
          $categoryFind  = Category::where('name',$value[7])->where('group_id',24)->first();
          $newPoint = new Point;
          $newPoint->name = $value[1] == null? ' ': $value[1];
          $newPoint->slug = $value[1] == null? ' ': $value[1];
          $newPoint->price_bhyt = $value[3] == null? '-': $value[3];
          $newPoint->price_dichvu = $value[4] == null? '-': $value[4];
          $newPoint->price_dichvutyc = $value[5] == null? '-': $value[5];
          $newPoint->group_id = 24;
          if(!isset($categoryFind)){
            $newCategory = new Category;
            $newCategory->name = $value[7];
            $newCategory->slug = $value[7];
            $newCategory->group_id = 24;
            $newCategory->save();
            $newPoint->category_id = $newCategory->id;
            } else {
              $newPoint->category_id = $categoryFind->id;
          }
          $newPoint->save();
        }
      }      
      return 'done';
    }

    // public function import_excel(Request $request){
    //   $this->validate($request, [
    //     'file' => 'required|mimes:xls,xlsx'
    //   ]);

    //   $path = $request->file('file');
    //   $array = Excel::toCollection(new ExcelImports, $path);
    //   foreach ($array as $key => $chidinh) {
    //     foreach ($chidinh as $key => $value) {          
    //       $groupFind  = Group::where('name',$value[5])->first();
    //       $categoryFind  = Category::where('name',$value[6])->first();
    //       $newPoint = new Point;
    //       $newPoint->name = $value[1] == null? ' ': $value[1];
    //       $newPoint->slug = $value[1] == null? ' ': $value[1];
    //       $newPoint->price_bhyt = $value[2] == null? 0: $value[2];
    //       $newPoint->price_dichvu = $value[3] == null? 0: $value[3];
    //       $newPoint->price_dichvutyc = $value[4] == null? 0: $value[4];
    //       if(!isset($groupFind)){
    //         $newGroup = new Group;
    //         $newGroup->name = $value[5];
    //         $newGroup->slug = $value[5];
    //         $newGroup->save();
    //         $newPoint->group_id = $newGroup->id;
    //       } else {
    //         $newPoint->group_id = $groupFind->id;
    //       }
    //       if(!isset($categoryFind)){
    //         $newCategory = new Category;
    //         $newCategory->name = $value[6];
    //         $newCategory->group_id = $newGroup->id;
    //         $newCategory->slug = $value[6];
    //         $newCategory->save();
    //         $newPoint->category_id = $newCategory->id;
    //       } else {
    //         $newPoint->category_id = $categoryFind->id;
    //       }
    //       $newPoint->save();
    //     }
    //   }      
    //   return 'done';
    // }

    public function export_excel(){
      return Excel::download(new ExcelExports, 'Point.xlsx');
    }

}
