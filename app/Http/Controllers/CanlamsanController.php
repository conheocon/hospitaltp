<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Point;
use App\Models\Category;
use App\Models\Group;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class CanlamsanController extends Controller
{
    //
    public function templatecanlamsan (){
      $point_group = Group::where('id',21)->first();
      $point_group_id = $point_group->id;
      $point_point = Point::where('group_id',$point_group_id)->paginate(10);
    	
      $point_category = Category::where('group_id',$point_group_id)->get();
      return view('canlamsan')->with(compact('point_category','point_point'));
    }
}
