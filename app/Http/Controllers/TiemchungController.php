<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Point;
use App\Models\Category;
use App\Models\Group;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class TiemchungController extends Controller
{
    //
    public function templatetiemchung (){
      $point_group = Group::where('id',24)->first();
      $point_group_id = $point_group->id;
      $point_point = Point::where('group_id',$point_group_id)->paginate(10);
    	
      $point_category = Category::where('group_id',$point_group_id)->get();
      return view('tiemchung')->with(compact('point_category','point_point'));
    }
}
