<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'db_chidinh';

    public function category() {
    	return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function group() {
    	return $this->belongsTo('App\Models\Group', 'group_id');
    }
}
