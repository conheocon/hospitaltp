<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'db_category';

    public function point() {
    	return $this->hasMany('App\Models\Point', 'id');
    }

    public function group() {
    	return $this->belongsTo('App\Models\Group', 'group_id');
    }
}
