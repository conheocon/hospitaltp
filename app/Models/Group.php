<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'db_group';

    public function point() {
    	return $this->hasMany('App\Models\Point', 'id');
    }

    public function category() {
    	return $this->hasMany('App\Models\Category', 'id');
    }
}
