<?php

namespace App\Imports;

use App\Models\Point;
use Maatwebsite\Excel\Concerns\ToModel;

class ExcelImports implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Point([
            'point_name' => $row[1],
            'point_slug' => $row[2],
            'point_bhyt' => $row[3],
            'point_dichvu' => $row[4],
            'point_dichvutyc' => $row[5],
            'group_id' => $row[6],
            'category_id' => $row[7],
        ]);
    }
}
