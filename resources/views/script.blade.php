<script type="text/javascript">
  $(document).ready(function(){
    $('#button_search').click(function(){
      var search_keywords = $('.search_keywords').val();
      var category_id = $('.category_id').val();
      var _token = $('input[name="_token"]').val();
      // console.log(search_keywords,category_id);
      $.ajax({
          url: "{{url('/search-keywords-point')}}",
          method: "POST",
          data:{search_keywords:search_keywords,category_id:category_id,_token:_token},
          success:function(data){
            // console.log(data.output,search_keywords);
            $('#search_data').html(data.output);
            $('#search-ajax').fadeOut();
          }
      });
    });
  });
</script>

<script type="text/javascript">
  $('#keywords').keyup(function(){
    var query = $(this).val();
      if(query != '' && query.length > 1){
        var _token = $('input[name="_token"]').val();
        let category = $('#exampleFormControlSelect1').val();
        $.ajax({
          url: "{{url('/autocomplete-ajax')}}",
          method: "POST",
          data:{query:query,category:category,_token:_token},
          success:function(data){
            $('#search-ajax').fadeIn().html(data);
          }
        });
      }else{
        window.onclick = myFunction;
        function myFunction() {
          $('#search-ajax').fadeOut();
        }
      }
  });

  $(document).on('click','.li_search_ajax',function(){
    $('#keywords').val($(this).attr('data-name'));
    $('#search-ajax').fadeOut();
    var query1 = $(this).attr('data-id');
    var _token = $('input[name="_token"]').val();
      $.ajax({
          url: "{{url('/autocomplete')}}",
          method: "POST",
          data:{query1:query1,_token:_token},
          success:function(data){
            $('#search_data').html(data.output);
          }
      });
  });
</script>
<script type="text/javascript">
  function check(){
     setTimeout(function(){
        document.getElementById('keywords').value = '';
      }, 150000);

     setTimeout(function(){
        document.getElementById('search-ajax').style.display = "none";
      }, 150000);
  }
</script>

<script type="text/javascript">
  function show(){
    setTimeout(function(){
        document.getElementById('exampleModalLong').style.display = "none";
      }, 150000);
  }
</script>

<script type="text/javascript">
  function dropdownmenu(){
    setTimeout(function(){
        document.getElementById('exampleModalLong').style.display = "none";
      }, 150000);
  }
  $(window).ready(function() { 
    $("#form-id").on("keypress", function (event) { 
        var keyPressed = event.keyCode || event.which; 
        if (keyPressed === 13) { 
            event.preventDefault(); 
            return false; 
        } 
    }); 
  });
</script>

<script type="text/javascript">
  window.onclick = myFunction;
  function myFunction() {
    $('#search-ajax').fadeOut();
  }
</script>

<script type="text/javascript">
function idleTimer() {
    var t;
    window.onload = resetTimer;
    window.onmousemove = resetTimer; // catches mouse movements
    window.onmousedown = resetTimer; // catches mouse movements
    window.onclick = resetTimer;     // catches mouse clicks
    window.onscroll = resetTimer;    // catches scrolling
    window.onkeypress = resetTimer;  //catches keyboard actions

    function reload() {
          window.location = '{{route('index')}}';  //Reloads the current page
    }

    function resetTimer(){
        clearTimeout(t);
        t = setTimeout(reload, 60000);  // time is in milliseconds (1000 is 1 second)
    }
}
idleTimer();
</script>



