@extends('layout')
@section('title','Cận Lâm Sàn')
@section('content')
<style type="text/css">
  .pager {
      font-size: 21px;
  }
</style>
<h3>Bạn đang tìm kiếm thông tin về @yield('title')</h3>
    <div class="container-fluid content">
        <div class="row">
            <div class="col-md-3 content1">
                <div id="form">
                    <h4>TÌM GIÁ DỊCH VỤ</h4>

                    <form action="#" autocomplete="off" method="post" id="form-id">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nhập tên dịch vụ</label>
                            <input type="text" name="search_keywords" class="form-control search_keywords" id="keywords" placeholder="Dịch vụ ...." oninput="check()">
                            <div id="search-ajax"></div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Khoa</label>
                            <select class="form-control category_id" id="exampleFormControlSelect1">
                               <option value="">---Chọn hết---</option>
                              @foreach ($point_category as $key => $category) 
                                <option value="{{$category->id}}">{{$category->name}}</option>
                              @endforeach
                            </select>
                        </div>

                        <!-- Button trigger modal -->
                        <input type="button" class="btn btn-warning" id="button_search" data-toggle="modal" data-target="#exampleModalLong" value="Tìm kiếm">
                    </form>

                        <!-- Modal -->
                        <div class="modal fade" data-toggle="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                          <div class="modal-dialog container" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Tên chỉ định</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              
                              <div id="search_data">

                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-warning" data-dismiss="modal" style="color: white">Đóng</button>
                              </div>
                            </div>
                          </div>
                        </div>

                    <div class="content2">
                       <h5>HỖ TRỢ</h5>
                       <div class="socical">
                           <i class="fas fa-phone mr-3" style="font-size:20px;color:white;"></i>
                           <p>(028) 54 088 924</p>
                       </div>
                    </div>
                </div>
                <div class="back">
                  <a href="{{route('index')}}"><button type="button" class="btn btn-primary">Back</button></a>
                </div>
            </div>
            <div class="col-md-9 content-left">
                <table class="table table-hover">
                    <thead class="content-font1">
                        <tr>
                            <th rowspan="2">Tên chỉ định (Theo phương tương đương)</th>
                            <th rowspan="2">Khoa</th>
                            <th colspan="3">Giá</th>
                        </tr>
                        <tr>
                            <th scope="col"> BHYT <br> (Theo TT13) </th>
                            <th scope="col"> Dịch vụ <br> (Theo TT14) </th>
                            <th scope="col">Dịch vụ theo <br> yêu cầu <br> (Ngoài giờ)</th>
                        </tr>
                        <tr class="one">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="content-font2">

                      @foreach ($point_point as $key => $value)
                        <tr>
                            <td scope="row">{{$value->name}}</td>
                            <td scope="row">{{$value->category->name}}</td>
                            <td class="price">{{$value->price_bhyt}}</td>
                            <td class="price">{{$value->price_dichvu}}</td>
                            <td class="price">{{$value->price_dichvutyc}}</td>
                        </tr>
                      @endforeach
                        
                    </tbody>
                </table>
                <div class="pagination">
                  {{ $point_point->links('vendor.pagination.custom') }}
                </div>
            </div>
        </div>
    </div>
    <script src="{{('frontend/js/jquery-3.5.1-jquery.min.js')}}"></script>
    @include('script')
    
@endsection