@extends('layout')
@section('title','Tân Phú Hospital')
@section('content')
    <div class="container content">
        <div class="row bottom">
          <div class="col-4">
            <a href="{{route('search_point')}}">
              <div class="card">
                  <div class="card-top">
                    <h2>Tra Cứu Nhanh</h2>
                  </div>
                  <a href="{{route('search_point')}}"><div class="card-bottom"><p>Tra cứu <i class="fas fa-arrow-right"></i></p></div></a>
              </div>
            </a>
          </div>
          <div class="col-4">
            <a href="{{route('canlamsan')}}">
              <div class="card">
                  <div class="card-top">
                    <h2>cận lâm sàng</h2>
                  </div>
                  <a href="{{route('canlamsan')}}"><div class="card-bottom"><p>Tra cứu <i class="fas fa-arrow-right"></i></p></div></a>
              </div>
            </a>
          </div>      
          <div class="col-4">
            <a href="{{asset('/')}}">
              <div class="card">
                  <div class="card-top">
                    <h2>Quy Trình</h2>
                  </div>
                  <a href="{{asset('/')}}"><div class="card-bottom"><p>Tra cứu <i class="fas fa-arrow-right"></i></p></div></a>
              </div>
            </a>
          </div>  
        </div>
        <br>
        <br>
        <div class="row top">
          <div class="col-4">
            <a href="{{route('khambenh')}}">
              <div class="card">
                  <div class="card-top">
                    <h2>Khám bệnh <br> khám sức khoẻ</h2>
                  </div>
                  <a href="{{route('khambenh')}}"><div class="card-bottom"><p>Tra cứu <i class="fas fa-arrow-right"></i></p></div></a>
              </div>
            </a>
          </div>      
          <div class="col-4">
            <a href="{{route('tiemchung')}}">
              <div class="card">
                  <div class="card-top">
                    <h2>tiêm chủng <br> giá phòng</h2>
                  </div>
                  <a href="{{route('tiemchung')}}"><div class="card-bottom"><p>Tra cứu <i class="fas fa-arrow-right"></i></p></div></a>
              </div>
            </a>
          </div>  
          <div class="col-4">
            <a href="{{route('phauthuat')}}">
              <div class="card">
                  <div class="card-top">
                    <h2>phẫu thuật <br> thủ thuật</h2>
                  </div>
                  <a href="{{route('phauthuat')}}"><div class="card-bottom"><p>Tra cứu <i class="fas fa-arrow-right"></i></p></div></a>
              </div>
            </a>
          </div>       
        </div>
    </div>
@endsection

