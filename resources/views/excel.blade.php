<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ImportExecl Hospital</title>
	<link rel="stylesheet" type="text/css" href="{{('frontend/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{('frontend/bootstrap/js/bootstrap.min.js')}}">
</head>
<body>

	  {{-- import data --}}
    <form action="{{URL::to('/import-excel')}}" method="POST" enctype="multipart/form-data" style="text-align: center; margin-top: 30px">
      @csrf
      <input type="file" name="file" accept=".xlsx"><br><br>
      <input type="submit" value="Import file Excel" name="import-csv" class="btn btn-warning">
    </form>
    <br>
    {{-- export data --}}
    <form action="{{URL::to('/export-excel')}}" method="POST" enctype="multipart/form-data" style="text-align: center">
      @csrf
      <input type="submit" value="Export file Excel" name="export-csv" class="btn btn-success">
    </form>

</body>
</html>