@extends('layout')

@section('content')
    <div class="container content">
        <div class="row">
            <div class="col-md-4 content1">
                <div id="form">
                    <h4>TÌM GIÁ DỊCH VỤ</h4>

                    <form action="{{URL::to('/search')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nhập tên dịch vụ</label>
                            <input type="text" name="search_keywords" class="form-control" id="exampleFormControlInput1">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Nhóm</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                               <option>Chọn hết</option>
                               <option>1</option>
                               <option>2</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Danh mục</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                               <option>Chọn hết</option>
                               <option>1</option>
                               <option>2</option>
                            </select>
                        </div>

                        <div class="search">
                          <!-- Button trigger modal -->
                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalLong" style="margin-left: 245px; margin-bottom: 10px; border-radius: 10px; color: white">
                            Tìm kiếm
                          </button>

                          <!-- Modal -->
                          <div class="modal fade container-fluid" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog container" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLongTitle">Tên chỉ định</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-hover">
                                        <thead class="content-font1">
                                            <tr>
                                                <th rowspan="2">Tên chỉ định (Theo phương tương đương)</th>
                                                <th colspan="3">Giá</th>
                                            </tr>
                                            <tr>
                                                <th scope="col"> BHYT <br> (Theo TT13) </th>
                                                <th scope="col"> Dịch vụ <br> (Theo TT14) </th>
                                                <th scope="col">Dịch vụ theo <br> yêu cầu <br> (Ngoài giờ)</th>
                                            </tr>
                                            <tr class="one">
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody class="content-font2">
                                            @foreach($search_point as $key =>$val)
                                                <tr>
                                                    <td scope="row">{{$val->name}}</td>
                                                    <td>{{$val->price_bhyt}}</td>
                                                    <td>{{$val->price_dichvu}}</td>
                                                    <td>{{$val->price_dichvutyc}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        {{-- <div class="button text-right">
                            <input type="submit" class="btn btn-warning" name="search_items" value="Tìm kiếm">
                        </div> --}}
                    </form>

                    <div class="content2">
                       <h5>HỖ TRỢ</h5>
                       <div class="socical">
                           <i class="fas fa-phone mr-3" style="font-size:20px;color:white;"></i>
                           <p>0123 456 789</p>
                       </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 content-left">
                <table class="table table-hover">
                    <thead class="content-font1">
                        <tr>
                            <th rowspan="2">Tên chỉ định (Theo phương tương đương)</th>
                            <th colspan="3">Giá</th>
                        </tr>
                        <tr>
                            <th scope="col"> BHYT <br> (Theo TT13) </th>
                            <th scope="col"> Dịch vụ <br> (Theo TT14) </th>
                            <th scope="col">Dịch vụ theo <br> yêu cầu <br> (Ngoài giờ)</th>
                        </tr>
                        <tr class="one">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="content-font2">
                        <tr>
                            <td scope="row">Khám bệnh trong giờ</td>
                            <td>34,500</td>
                            <td>35,500</td>
                            <td></td>
                        </tr>
                        
                    </tbody>
                </table>
                <div class="pagination">
                  <button type="button" class="btn btn-warning mr-2">Trước</button>
                  <a href="#">1</a>
                  <a href="#">2</a>
                  <a href="#">3</a>
                  <a href="#">4</a>
                  <a href="#">5</a>
                  <p>...</p>
                  <a href="#">6</a>
                  <button type="button" class="btn btn-warning ml-2">Sau</button>
                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
        @media (min-width: 576px){
            .modal-dialog {
                max-width: 100% !important;
                margin: 1.75rem auto;
            }
        }
    </style>
    <script type="text/javascript">

      $(window).on('load',function(){
        setTimeout(function(){
          $('#exampleModalLong').modal('show')
        }, 45000);
      });
    </script>
    @include('script')
    
@endsection