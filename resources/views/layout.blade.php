<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{('frontend/css/Hospital.css')}}">
    <link rel="stylesheet" type="text/css" href="{{('frontend/bootstrap/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{('frontend/bootstrap/js/bootstrap.min.js')}}">
    <link rel="stylesheet" type="text/css" href="{{('frontend/fontawesome/css/all.css')}}">
    <link rel="stylesheet" type="text/css" href="{{('frontend/css/layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{('frontend/css/allpage.css')}}">

    <script type="text/javascript" src="{{('frontend/js/jquery-3.2.1.slim.min.js')}}"></script>
    <script type="text/javascript" src="{{('frontend/js/js-popper.min.js')}}"></script>
    <script type="text/javascript" src="{{('frontend/js/js-bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{('frontend/js/js-sweetalert.min.js')}}"></script>

    {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
</head>
<body>
    <a href="{{route('index')}}">
      <div class="banner-top">
          <img src="{{('frontend/image/bv1.jpg')}}">
      </div>
    </a>
    @yield('content')
</body>
</html>