@extends('admin_layout')
@section('content')
@section('title','Quy Trình')
  <form action="{{URL::to('point/import-pdf')}}" method="POST" enctype="multipart/form-data" style="text-align: center; margin-top: 10px">
    @csrf
    <input type="file" name="file" accept=".pdf"><br><br>
    <input style="border-color: black" type="submit" value="Import file PDF" name="import-csv" class="btn btn-warning">
  </form>

  <br>
    <?php
      $message = Session::get('message');
      if($message){
          echo '<span class="text-alert" style="color: red; font-weight: bold; margin-left: 30px; font-size: 19px;">'.$message.'</span>';
          Session::put('message',null); 
      }
    ?>
  <br><br>

  <div class="container">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">STT</th>
          <th scope="col">File qui trình</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        @foreach($file as $key => $data)
          <tr>
            <th scope="row">{{++$key}}</th>
            <td><a href="{{route('view_files',['files_id'=>$data->id])}}">{{$data->file}}</a></td>
            <td>
            {{-- <td><a href="{{asset('storage/'.$data->file)}}">{{$data->file}}</a></td> --}}
            <td>
              <a onclick="return confirm('Bạn có chắc là muốn xóa chỉ định này không?')" href="{{route('delete_viewpdf',['files_id'=>$data->id])}}" class="active styling-edit" ui-toggle-class="">
                <i class="fas fa-trash-alt text-danger text"></i>
              </a>
          </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <br>
  <div class="paginations">
    {{ $file->links('vendor.pagination.custom') }}
  </div>
@endsection