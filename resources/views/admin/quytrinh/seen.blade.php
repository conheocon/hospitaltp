@extends('admin_layout')
@section('content')
@section('title','Quy Trình')
  <div class="container" style="text-align: right; padding-right: 40px">
	 <a href="{{route('view_uploadpdf')}}"><button type="button" class="btn btn-primary">Back</button></a>
  </div>
  <br>
  <div class="container" style="text-align: center">
    <iframe src="{{url('storage/'.$data->file)}}" style="width: 1050px; height: 800px"></iframe>
  </div>
  <br>
@endsection