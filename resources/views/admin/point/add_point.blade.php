@extends('admin_layout')
@section('content')
@section('title','Chỉ Định')
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">Thêm chỉ định</h3>
               </div>
               <?php
                  $message = Session::get('message');
                  if($message){
                      echo '<span class="text-alert">'.$message.'</span>';
                      Session::put('message',null); 
                  }
               ?>
               <!-- /.card-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin_addpoint')}}" method="post">
                  {{csrf_field()}}
                  <div class="card-body">
                     <div class="form-group1">
                        <label for="exampleInputEmail1">Tên Chỉ định</label>
                        <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Tên chỉ định">
                     </div>
                     <div class="form-group1">
                        <label for="exampleInputEmail1">Giá BHYT</label>
                        <input type="text" name="price_bhyt" class="form-control" id="exampleInputEmail1" placeholder="100,000 đ">
                     </div>
                     <div class="form-group1">
                        <label for="exampleInputEmail1">Giá dịch vụ</label>
                        <input type="text" name="price_dichvu" class="form-control" id="exampleInputEmail1" placeholder="100,000 đ">
                     </div>
                     <div class="form-group1">
                        <label for="exampleInputEmail1">Giá dịch vụ theo yêu cầu</label>
                        <input type="text" name="price_dichvutyc" class="form-control" id="exampleInputEmail1" placeholder="100,000 đ">
                     </div>
                     <div class="form-group1">
                         <label for="exampleInputPassword1">Nhóm</label>
                         <select id="group_point" name="group_id" class="form-control input-sm m-bot15 choose nhom" onchange="changepoint()">
                              <option value="">--Chọn nhóm--</option>
                             @foreach($point_group as $key => $group)
                              <option value="{{$group->id}}">{{$group->name}}</option>
                             @endforeach
                         </select>  
                     </div>
                     <div class="form-group1">
                         <label for="exampleInputPassword1">Khoa</label>
                         <select id="khoa" name="category_id" class="form-control input-sm m-bot15 khoa">
                              <option value="">--Chọn khoa--</option>

                         </select>
                     </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <button type="submit" class="btn btn-primary">Thêm</button>
                  </div>
               </form>
               <div class="card-footer">
                  <button type="submit" class="btn btn-danger"><a href="{{route('show_point')}}"><font color="white">Hủy bỏ</font></a></button>
               </div>
            </div>
            <!-- /.card -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <div class="col-md-6">
         </div>
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>
@endsection