@extends('admin_layout')
@section('content')
@section('title','Danh sách chỉ định')
<style type="text/css">
  .cot{text-align: center;font-size: 20px;font-weight: bold;color: crimson;}
  .quaylai{margin-left: 1046px;}
</style>
<div class="container">
  <form action="{{route('admin_searchpoint')}}" autocomplete="off" method="post" id="form-id" style="position: relative">
      {{csrf_field()}}
      <div class="form-group" >
          <label for="exampleFormControlInput1">Nhập tên chỉ định</label>
          <input type="text" name="search_keywords" class="form-control search_keywords" id="keywords1" placeholder="Dịch vụ ....">
          <div id="search-ajax1"></div>
      </div>
      <div class="form-group">
          <label for="exampleFormControlSelect1">Khoa</label>
          <select class="form-control category_id khoa1" id="exampleFormControlSelect1" name="khoa_name">
             <option value="">---Chọn hết---</option>
            @foreach ($point_category as $key => $category)
              <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
          </select>
      </div>

      <!-- Button trigger modal -->
      <input type="submit" class="btn btn-warning" id="button_search" data-toggle="modal" data-target="#exampleModalLong" value="Tìm kiếm">
  </form>
<div>
<br>
<div class="container quaylai">
  <a href="{{route('show_point')}}"><button type="button" class="btn btn-primary">Back</button></a>
</div>
<!-- /.card-header -->
<?php
    $message = Session::get('message');
    if($message){
        echo '<span class="text-alert">'.$message.'</span>';
        Session::put('message',null); 
    }
  ?>
<div class="card-body">
  <table id="example2" class="table table-bordered table-hover">
    <thead>
    <tr>
      <th>Tên chỉ định</th>
      <th width="10%">Giá BHYT</th>
      <th width="13%">Giá dịch vụ</th>
      <th width="15%">Giá dịch vụ tyc</th>
      <th>Nhóm</th>
      <th>Khoa</th>
      <th width="7%"></th>
    </tr>
    </thead>
    <tbody>
    @if($data_keywords->count()>0)
      @foreach($data_keywords as $key => $datakeywords)
        <tr>
            <td>{{$datakeywords->name}}</td>
            <td>{{$datakeywords->price_bhyt}}</td>
            <td>{{$datakeywords->price_dichvu}}</td>
            <td>{{$datakeywords->price_dichvutyc}}</td>
            <td>{{$datakeywords->group->name}}</td>
            <td>{{$datakeywords->category->name}}</td>
            <td>
              <a href="{{route('edit_point',['point_id'=>$datakeywords->id])}}" class="active styling-edit" style="margin-right: 10px">
                <i class="far fa-edit"></i>
              </a>
              <a onclick="return confirm('Bạn có chắc là muốn xóa chỉ định này không?')" href="{{route('delete_point',['point_id'=>$datakeywords->id])}}" class="active styling-edit" ui-toggle-class="">
                <i class="fas fa-trash-alt text-danger text"></i>
              </a>
            </td>
        </tr>
      @endforeach
    @else
      <tr class="cot">
          <td colspan="7">Không tìm thấy chỉ định nào</td>
      </tr>
    @endif
    </tbody>
  </table><br>
  {{-- <div class="paginations">
    {{ $data_keywords->links('vendor.pagination.custom') }}
  </div> --}}
</div>
@endsection